import { Component, signal } from '@angular/core';
import { RouterOutlet } from '@angular/router';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-labs',
  standalone: true,
  imports : [RouterOutlet, CommonModule],
  templateUrl: './labs.component.html',
  styleUrl: './labs.component.css'
})
export class LabsComponent {
  title = 'todoapp';
  tasks = signal([
    'Install Angular CLI',
    'Create Project',
    'Create Components'
  ]);
  name = signal('Markus');
  age=18;
  disabled=true;
  person = {
    name: 'nicolas',
    age: 22,
    avatar:'https://w3schools.com/howto/img_avatar.png'
  }

  clickHandler(){
    console.log('hola')
  }

  changeHandler(event: Event){
    const input = event.target as HTMLInputElement;
    const newValue = input.value
    //modificar signal, no poder asignar directamente
    this.name.set(newValue)

    console.log(event)
  }
  keydownHandler(event: KeyboardEvent){
    const input = event.target as HTMLInputElement;
    console.log(input.value)
  }
}
